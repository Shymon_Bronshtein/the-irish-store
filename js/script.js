$(document).ready(function () {
    $('.picks__list').slick({
        arrows: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 2
    });
})